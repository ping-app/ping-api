package com.statefarm.pingpong;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class PingPongServiceTests {

    private static PingPongService pingPongService;

    @BeforeAll
    static void beforeAll() {
        pingPongService = new PingPongService();
    }

    @Test
    void returnDate() {
        Date result = pingPongService.getTimeStamp();
        assertThat(result).isNotNull();
        assertThat(result.toString().isEmpty()).isFalse();

    }
}
