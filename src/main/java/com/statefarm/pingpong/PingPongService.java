package com.statefarm.pingpong;

import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PingPongService {
    public Date getTimeStamp() {
        return new Date();
    }
}
